package helloworld

import (
	"fmt"
)

func HelloWorld() {
	fmt.Println("Hello World!")
}

func Hello(name string) {
	fmt.Printf("Hello %s\n", name)
}
